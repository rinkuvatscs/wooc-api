package com.woocation.util;

import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.apache.commons.lang.WordUtils;
import org.joda.time.Days;

import com.google.common.base.Splitter;

/**
 * The Class WooDateUtils.
 */
public class WooDateUtils {

	/**
	 * Gets the month.
	 *
	 * @param date
	 *            the date
	 * @return the month
	 */
	public static String getMonth(final String date) {
		List<String> dateAttrib = Splitter.on("-").splitToList(date);
		String month = Month.of(Integer.parseInt(dateAttrib.get(1))).name();
		if (month != null) {
			month = month.toLowerCase();
			month = WordUtils.capitalize(month);
		}
		return month;
	}

	/**
	 * Gets the date.
	 *
	 * @param date
	 *            the date
	 * @return the date
	 * @throws Exception
	 *             the exception
	 */
	public static LocalDate getDate(final String date) throws Exception {
		LocalDate localDate = null;
		List<String> dateAttrib = Splitter.on("-").splitToList(date);
		localDate = LocalDate.of(Integer.parseInt(dateAttrib.get(2)), Integer.parseInt(dateAttrib.get(1)),
				Integer.parseInt(dateAttrib.get(0)));
		if (localDate == null) {
			throw new Exception("Please enter date in dd-MM-YYYY format.");
		}
		return localDate;
	}

	/**
	 * Validate date.
	 *
	 * @param date
	 *            the date
	 * @return true, if successful
	 * @throws Exception
	 *             the exception
	 */
	public static boolean validateDate(final String date) throws Exception {
		LocalDate localDate = getDate(date);
		if (localDate.isBefore(LocalDate.now())) {
			throw new Exception("Before date is not valid");
		}
		return true;
	}

	/**
	 * Format date.
	 *
	 * @param localDate
	 *            the local date
	 * @return the string
	 */
	public static String formatDate(LocalDate localDate) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		return localDate.format(formatter);
	}

	/**
	 * Gets the duration.
	 *
	 * @param startDate
	 *            the start date
	 * @param endDate
	 *            the end date
	 * @return the duration
	 * @throws Exception
	 *             the exception
	 */
	public static Integer getDuration(String startDate, String endDate) throws Exception {
		LocalDate start = getDate(startDate);
		LocalDate end = getDate(endDate);
		return (int) java.time.temporal.ChronoUnit.DAYS.between(start, end);
	}
}
