/**
 * 
 */
package com.woocation.util;

import com.woocation.inference.response.TravelCityDetails;
import com.woocation.inference.response.TravelCityDetailsCompact;


/**
 * @author Ankit.Gupta
 *
 */
public class TravelCompactedConversion {

	/**
	 * 
	 */
	public TravelCompactedConversion() {
	}

	public static TravelCityDetailsCompact getCompactedTravelDetailsResponse(TravelCityDetails dResp) {
		TravelCityDetailsCompact response = new TravelCityDetailsCompact();
		/*if (dResp.getFromCity() != null) {
	public static TravelDetailsCompactResponse getCompactedTravelDetailsResponse(TravelDetailsResponse dResp) {
		TravelDetailsCompactResponse response = new TravelDetailsCompactResponse();
		if (dResp.getFromCity() != null) {
			response.setFc(Joiner.on("|").join(dResp.getFromCity(), dResp.getFromState(), dResp.getFromCountry()));
		}

		if (dResp.getToCity() != null) {
			response.setTc(Joiner.on("|").join(dResp.getToCity(), dResp.getToState(), dResp.getToCountry()));
		}

		response.setEu(dResp.getIsEuropean());
		response.setBeach(dResp.getIsBeach());
		response.setCold(dResp.getGoingToCold());
		response.setHot(dResp.getGoingToHot());
		response.setCur(dResp.getToCityCurrency());
		response.setDist(dResp.getDistance());
		response.setSnow(dResp.getSnowPossible());

		if (CollectionUtils.isNotEmpty(dResp.getNearestAirport())) {
			response.setNbc(Joiner.on("|").join(dResp.getNearestBigCity()));
		}

		if (dResp.getFromDaysStats() != null && CollectionUtils.isNotEmpty(dResp.getFromDaysStats().values())) {
			StringBuilder fromStats = new StringBuilder();

			for (boolean value : dResp.getFromDaysStats().values()) {
				fromStats.append(value ? "1" : "0").append("|");
			}
			response.setFrStats(fromStats.toString());
		}

		if (dResp.getToDaysStats() != null && CollectionUtils.isNotEmpty(dResp.getToDaysStats().values())) {
			StringBuilder toStats = new StringBuilder();

			for (boolean value : dResp.getToDaysStats().values()) {
				toStats.append(value ? "1" : "0").append("|");
			}
			response.setToStats(toStats.toString());
		}

		if (CollectionUtils.isNotEmpty(dResp.getNearestAirport())) {
			response.setAirports(
					dResp.getNearestAirport().stream().map(airport -> airport.toString()).collect(Collectors.toList()));
		}

		if (dResp.getHotel() != null) {
			response.setHotel(dResp.getHotel().toString());*/
		return response;
	}
}
