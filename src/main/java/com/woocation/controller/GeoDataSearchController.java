package com.woocation.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.collect.Lists;
import com.woocation.dto.CityDTO;
import com.woocation.dto.CityEsDTO;
import com.woocation.dto.CitySuggestDTO;
import com.woocation.dto.GeoDTO;
import com.woocation.model.JetLagInfo;
import com.woocation.service.GeoDataSearchService;
import com.woocation.service.response.GeoIPLocationResponse;
import com.woocation.service.response.Response;
import com.woocation.service.response.ResponseBuilder;
import com.woocation.util.PropertyEnum;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api")
public class GeoDataSearchController {

	/** The logger. */
	private static final Logger logger = LoggerFactory.getLogger(GeoDataSearchController.class);

	@Autowired
	private GeoDataSearchService geoDataSearchService;

	/**
	 * Search query time.
	 *
	 * @param flightQuery
	 *            the flight query
	 * @return the response entity
	 */
	@ApiOperation(value = "Geo Search", notes = "Geo Search City")
	@RequestMapping(value = "/city/{geoNameId}", method = RequestMethod.GET)
	public Response<CityDTO> geoCity(@PathVariable Long geoNameId) {
		Response<CityDTO> response = null;
		try {
			CityDTO cityDTO = geoDataSearchService.searchCity(geoNameId);
			response = ResponseBuilder.buildOkResponse(Lists.newArrayList(cityDTO), HttpStatus.OK, "");
		} catch (Exception e) {
			return ResponseBuilder.buildErrorResponse(HttpStatus.BAD_REQUEST, e.getMessage());
		}
		return response;
	}
	
	@ApiOperation(value = "Get Geo City", notes = "Get Geo City")
	@RequestMapping(value = "/getCity/{geoNameId}", method = RequestMethod.GET)
	public Response<CityEsDTO> getGeoCity(@PathVariable Long geoNameId, @RequestParam(value = "field", required = false) PropertyEnum field) {
		Response<CityEsDTO> response = null;
		try {
			CityEsDTO cityEsDTO = geoDataSearchService.getCity(geoNameId, field);
			response = ResponseBuilder.buildOkResponse(Lists.newArrayList(cityEsDTO), HttpStatus.OK, "");
		} catch (Exception e) {
			return ResponseBuilder.buildErrorResponse(HttpStatus.BAD_REQUEST, e.getMessage());
		}
		return response;
	}

	/**
	 * Search query time.
	 *
	 * @param flightQuery
	 *            the flight query
	 * @return the response entity
	 */
	@ApiOperation(value = "Geo Search on AsciiName", notes = "Geo Search City by AsciiName")
	@RequestMapping(value = "/city", method = RequestMethod.GET)
	public Response<CityDTO> getCity(@RequestParam(value = "name") String name) {
		Response<CityDTO> response = null;
		try {
			CityDTO cityDTO = geoDataSearchService.searchCityByName(name);
			response = ResponseBuilder.buildOkResponse(Lists.newArrayList(cityDTO), HttpStatus.OK, "");
		} catch (Exception e) {
			return ResponseBuilder.buildErrorResponse(HttpStatus.BAD_REQUEST, e.getMessage());
		}
		return response;
	}

	/**
	 * Search query time.
	 *
	 * @param flightQuery
	 *            the flight query
	 * @return the response entity
	 */
	@ApiOperation(value = "Geo Search on CountryName", notes = "Geo Search by CountryName")
	@RequestMapping(value = "/geoAll", method = RequestMethod.GET)
	public Response<GeoDTO> getGeoList(@RequestParam(value = "countryName") String countryName) {
		Response<GeoDTO> response = null;
		try {
			List<GeoDTO> geoDtoList = geoDataSearchService.geoList(countryName);
			response = ResponseBuilder.buildOkResponse(geoDtoList, HttpStatus.OK, "");
		} catch (Exception e) {
			return ResponseBuilder.buildErrorResponse(HttpStatus.BAD_REQUEST, e.getMessage());
		}
		return response;
	}

	/**
	 * Search query time.
	 *
	 * @param flightQuery
	 *            the flight query
	 * @return the response entity
	 */
	@ApiOperation(value = "Geo Search on city and Country Name", notes = "Geo Search City by name and country code")
	@RequestMapping(value = "/cityByCountry", method = RequestMethod.GET)
	public Response<CityDTO> getCityByCountry(@RequestParam(value = "name") String name,
			@RequestParam(value = "country", required = false) String country,
			@RequestParam(value = "cc", required = false) String countryCode) {
		Response<CityDTO> response = null;
		try {

			if (StringUtils.isEmpty(country)) {
				country = "";

			}
			if (StringUtils.isEmpty(countryCode)) {
				countryCode = "";
			}

			List<CityDTO> cityDTO = geoDataSearchService.searchCityByCountry(name, country, countryCode);
			response = ResponseBuilder.buildOkResponse(cityDTO, HttpStatus.OK, "");
		} catch (Exception e) {
			return ResponseBuilder.buildErrorResponse(HttpStatus.BAD_REQUEST, e.getMessage());
		}
		return response;
	}

	/**
	 * Search query time.
	 *
	 * @param flightQuery
	 *            the flight query
	 * @return the response entity
	 */
	@ApiOperation(value = "Geo Search City", notes = "Geo Search City by lat and longitude")
	@RequestMapping(value = "/getCityByGeoPoint", method = RequestMethod.GET)
	public Response<CityDTO> getCityByGeoPoint(@RequestParam(value = "geoPoint") String geoPoint,
			@RequestParam(value = "distance", required = false) String distance) {
		Response<CityDTO> response = null;
		try {
			if (StringUtils.isEmpty(distance)) {
				distance = "10";
			}
			List<CityDTO> cityDTO = geoDataSearchService.searchCityByGeoPoint(geoPoint, distance);
			response = ResponseBuilder.buildOkResponse(cityDTO, HttpStatus.OK, "");
		} catch (Exception e) {
			return ResponseBuilder.buildErrorResponse(HttpStatus.BAD_REQUEST, e.getMessage());
		}
		return response;
	}

	/**
	 * Search query time.
	 *
	 * @param flightQuery
	 *            the flight query
	 * @return the response entity
	 */
	@ApiOperation(value = "Geo Search on AsciiName auto complete", notes = "Geo Search City by AsciiName initials")
	@RequestMapping(value = "/citySuggest", method = RequestMethod.GET)
	public Response<CitySuggestDTO> citySuggest(@RequestParam(value = "name") String name) {
		Response<CitySuggestDTO> response = null;
		try {
			List<CitySuggestDTO> listCitySuggest = geoDataSearchService.citySuggest(name);
			response = ResponseBuilder.buildOkResponse(listCitySuggest, HttpStatus.OK, "");
		} catch (Exception e) {
			return ResponseBuilder.buildErrorResponse(HttpStatus.BAD_REQUEST, e.getMessage());
		}
		return response;
	}

	/**
	 * Search query time.
	 *
	 * @param flightQuery
	 *            the flight query
	 * @return the response entity
	 */
	@ApiOperation(value = "Geo Search on IP Address", notes = "Geo Search City by IP Address")
	@RequestMapping(value = "/cityByIP", method = RequestMethod.GET)
	public Response<GeoIPLocationResponse> cityByIP(@RequestParam(value = "ip", required = false) String ipAddress, HttpServletRequest request) {
		Response<GeoIPLocationResponse> response = null;
		try {
			if(ipAddress == null){
				String remoteAddress = request.getRemoteAddr();
				System.out.println("Remote Addr  -> " + remoteAddress);
				System.out.println("Remote Host  -> " + request.getRemoteHost());
				if(remoteAddress != null){
					ipAddress = remoteAddress;
				}else{
					return ResponseBuilder.buildErrorResponse(HttpStatus.BAD_REQUEST, "Provide valid IP Address");
				}
			}
			GeoIPLocationResponse geoResponse = geoDataSearchService.searchCityByIP(ipAddress);
			response = ResponseBuilder.buildOkResponse(Lists.newArrayList(geoResponse), HttpStatus.OK, "");
		} catch (Exception e) {
			return ResponseBuilder.buildErrorResponse(HttpStatus.BAD_REQUEST, e.getMessage());
		}
		return response;
	}

	/**
	 * Search query time.
	 *
	 * @param flightQuery
	 *            the flight query
	 * @return the response entity
	 */
	@ApiOperation(value = "Jet Lag", notes = "Jet Lag recovery information")
	@RequestMapping(value = "/getJetLag", method = RequestMethod.GET)
	public Response<JetLagInfo> getJetLag(@RequestParam(value = "to") Long toGeoId,
			@RequestParam(value = "from") Long fromGeoId) {
		Response<JetLagInfo> response = null;
		try {
			JetLagInfo jetLagInfo = geoDataSearchService.getJetLag(toGeoId, fromGeoId);
			response = ResponseBuilder.buildOkResponse(Lists.newArrayList(jetLagInfo), HttpStatus.OK, "");
		} catch (Exception e) {
			return ResponseBuilder.buildErrorResponse(HttpStatus.BAD_REQUEST, e.getMessage());
		}
		return response;
	}

}
