package com.woocation.controller;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.woocation.dto.HotelSuggestDTO;
import com.woocation.model.BookingHotel;
import com.woocation.service.HotelUploadService;
import com.woocation.service.request.DataUploadRequest;
import com.woocation.service.response.GeoDataUploadResponse;
import com.woocation.service.response.Response;
import com.woocation.service.response.ResponseBuilder;

import io.swagger.annotations.ApiOperation;

/**
 * The Class HotelUploadController.
 */
@RestController
@RequestMapping("/api")
public class HotelUploadController {

	/** The logger. */
	private static final Logger logger = LoggerFactory.getLogger(HotelUploadController.class);

	/** The hotel service. */
	@Autowired
	private HotelUploadService hotelService;

	/**
	 * Search query time.
	 *
	 * @param uploadRequest
	 *            the upload request
	 * @return the response entity
	 */
	@ApiOperation(value = "Hotel Data Upload", notes = "Hotel Data Upload")
	@RequestMapping(value = "/uploadHotelData", method = RequestMethod.POST)
	public ResponseEntity<GeoDataUploadResponse> uploadHotelData(@RequestBody DataUploadRequest uploadRequest) {
		GeoDataUploadResponse uploadResponse = null;
		try {
			uploadResponse = hotelService.uploadHotelData(uploadRequest);
		} catch (Exception e) {
			new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(uploadResponse, HttpStatus.OK);
	}

	/**
	 * Hotel by geo point.
	 *
	 * @param name
	 *            the name
	 * @param geoPoint
	 *            the geo point
	 * @param distance
	 *            the distance
	 * @return the response
	 */
	@ApiOperation(value = "Hotel Search ", notes = "Search HOtel by lat and longitude")
	@RequestMapping(value = "/hotelByGeoPoint", method = RequestMethod.GET)
	public Response<BookingHotel> hotelByGeoPoint(@RequestParam(value = "name", required = false) String name,
			@RequestParam(value = "geoPoint") String geoPoint,
			@RequestParam(value = "distance", required = false) String distance) {
		Response<BookingHotel> response = null;
		try {
			if (StringUtils.isEmpty(distance)) {
				distance = "10";
			}
			List<BookingHotel> hotelList = hotelService.searchHotelByGeoPoint(name, geoPoint, distance);
			response = ResponseBuilder.buildOkResponse(hotelList, HttpStatus.OK, "");
		} catch (Exception e) {
			return ResponseBuilder.buildErrorResponse(HttpStatus.BAD_REQUEST, e.getMessage());
		}
		return response;
	}

	/**
	 * Search query time.
	 *
	 * @param hotelText
	 *            the hotel text
	 * @return the response entity
	 */
	@ApiOperation(value = "Suggest Data", notes = "Get Auto complete")
	@RequestMapping(value = "/hotelSuggest", method = RequestMethod.GET)
	public Response<HotelSuggestDTO> hotelSuggest(@RequestParam(value = "name") String hotelText) {
		Response<HotelSuggestDTO> response = null;
		try {
			List<HotelSuggestDTO> suggestDto = hotelService.suggest(hotelText);
			response = ResponseBuilder.buildOkResponse(suggestDto, HttpStatus.OK, "");
		} catch (Exception e) {
			return ResponseBuilder.buildErrorResponse(HttpStatus.BAD_REQUEST, e.getMessage());
		}
		return response;
	}
}
