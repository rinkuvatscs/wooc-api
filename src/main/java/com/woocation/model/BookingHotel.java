package com.woocation.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.google.common.base.Joiner;
import com.woocation.elastic.bean.EsBean;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Instantiates a new booking hotel.
 */
@Data
@EqualsAndHashCode(callSuper = false)
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class BookingHotel extends EsBean {

	/** The hotel id. */
	private Long hotelId;
	
	/** The name. */
	private String name;
	
	/** The hotel suggest. */
	private List<String> hotelSuggest;

	/** The property type. */
	private String propertyType;
	
	/** The location. */
	private Location location;

	/** The city. */
	private String city;

	/** The country. */
	private String country;

	/** The street. */
	private String street;
	
	/** The locality. */
	private String locality;
	
	/** The languages spoken. */
	private List<String> languagesSpoken;
	
	/** The amenities id. */
	private List<Integer> amenitiesId;

	/**
	 * Sets the hotel suggest.
	 */
	public void setHotelSuggest(){
		List<String> suggestList = new ArrayList<>();
//		suggestList.add(name);
//		suggestList.add(name + " " + city);
		suggestList.add(name + " " + city + " " + country);
		this.setHotelSuggest(suggestList);
	}
	
	@Override
	public String toString() {
		return Joiner.on("|").join(hotelId, name, propertyType);
	}

}
