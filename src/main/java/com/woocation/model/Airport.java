package com.woocation.model;

import com.google.common.base.Joiner;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The Class Airport.
 */
@Data
@NoArgsConstructor
public class Airport {

	/** The iata. */
	private String iata;

	/** The domestic count. */
	private Integer domesticCount;

	/** The international count. */
	private Integer internationalCount;

	/** The location. */
	private LocationRef location;

	@Override
	public String toString() {
		return Joiner.on("|").join(iata, domesticCount, internationalCount);
	}

}
