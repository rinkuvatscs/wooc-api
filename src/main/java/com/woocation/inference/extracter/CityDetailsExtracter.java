package com.woocation.inference.extracter;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.woocation.dao.GeoDataSearchDao;
import com.woocation.dto.AirportDTO;
import com.woocation.inference.WeatherCityParam;
import com.woocation.inference.WeatherMonthParam;
import com.woocation.inference.response.TravelCityDetails;
import com.woocation.inference.response.TravelCityParam;
import com.woocation.model.Airport;
import com.woocation.model.CityEsBean;
import com.woocation.model.Holiday;
import com.woocation.model.LanguageDetails;
import com.woocation.model.Languages;
import com.woocation.model.WeatherTypeParam;
import com.woocation.service.request.TravelRequest;
import com.woocation.util.DayUtils;
import com.woocation.util.EntityToDTO;
import com.woocation.util.WooDateUtils;

/**
 * The Class CityDetailsExtracter.
 */
@Component
public class CityDetailsExtracter {

	/** The geo data search dao. */
	@Autowired
	private GeoDataSearchDao geoDataSearchDao;

	/**
	 * Extract travel city details.
	 *
	 * @param travelRequest
	 *            the travel request
	 * @param fromCity
	 *            the from city
	 * @param toCity
	 *            the to city
	 * @return the travel city details
	 */
	public TravelCityParam extractTravelCityDetails(TravelRequest travelRequest, CityEsBean fromCity,
			CityEsBean toCity) {
		TravelCityParam cityParam = new TravelCityParam();

		TravelCityDetails fromCityDetails = new TravelCityDetails();
		TravelCityDetails toCityDetails = new TravelCityDetails();
		try {
			String travelMonth = WooDateUtils.getMonth(travelRequest.getStartDate());

			setFromCityFields(travelMonth, fromCity, fromCityDetails);
			setToCityFields(travelMonth, toCity, toCityDetails);
			if (toCity != null) {
				setNearestCity(toCity, toCityDetails);
			}
			setLanguage(fromCity, toCity, fromCityDetails, toCityDetails);
			setHolidays(fromCity, toCity, travelRequest, fromCityDetails, toCityDetails);
			setAirport(fromCity, toCity, fromCityDetails, toCityDetails);
			setCuisines(fromCity, toCity, fromCityDetails, toCityDetails);
			
			cityParam.setFromCityDetails(fromCityDetails);
			cityParam.setToCityDetails(toCityDetails);
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		return cityParam;
	}
	
	private void setCuisines(CityEsBean fromCity, CityEsBean toCity, TravelCityDetails fromCityDetails, TravelCityDetails toCityDetails){
		List<String> fromCuisine = fromCity.getCuisines().stream().map(item -> item.get(0)).collect(Collectors.toList());
		fromCityDetails.setCuisines(fromCuisine);
		
		List<String> toCuisine = toCity.getCuisines().stream().map(item -> item.get(0)).collect(Collectors.toList());
		toCityDetails.setCuisines(toCuisine);
	}

	/**
	 * Sets the weather data.
	 *
	 * @param city
	 *            the city
	 * @param travelMonth
	 *            the travel month
	 * @param response
	 *            the response
	 * @return the weather city param
	 */
	private WeatherCityParam setWeatherData(CityEsBean city, String travelMonth, TravelCityDetails response) {
		WeatherCityParam whParam = new WeatherCityParam();
		Map<Integer, WeatherMonthParam> monthParamMap = extractWeatherMonthParams(city);

		Integer wt_12_min = 0;
		Integer wt_12_max = 0;
		Integer wt_past_3_min = 0;
		Integer wt_past_3_max = 0;
		Integer wt_past_1_min = 0;
		Integer wt_past_1_max = 0;

		Boolean wt_12_snow = null;

		Month month = Month.valueOf(travelMonth.toUpperCase());

		for (Map.Entry<Integer, WeatherMonthParam> entity : monthParamMap.entrySet()) {
			WeatherMonthParam monthParam = entity.getValue();
			if (wt_12_min == 0) {
				wt_12_min = monthParam.getMin();
			}
			if (wt_12_min > monthParam.getMin()) {
				wt_12_min = monthParam.getMin();
			}

			if (wt_12_max == 0) {
				wt_12_max = monthParam.getMax();
			}
			if (wt_12_max < monthParam.getMax()) {
				wt_12_max = monthParam.getMax();
			}

			if (wt_12_snow == null && monthParam.getSnow()) {
				wt_12_snow = monthParam.getSnow();
			}
		}

		Integer last3Month = month.minus(3).getValue();
		Integer i = 0;
		while (i < 3) {

			int monthCount = last3Month + i;
			if (monthCount > 12) {
				monthCount = 0 + i;
			}

			WeatherMonthParam lastMonth = monthParamMap.get(monthCount);
			if (wt_past_3_min == 0) {
				wt_past_3_min = lastMonth.getMin();
			}
			if (wt_past_3_min > lastMonth.getMin()) {
				wt_past_3_min = lastMonth.getMin();
			}

			if (wt_past_3_max == 0) {
				wt_past_3_max = lastMonth.getMax();
			}
			if (wt_past_3_max < lastMonth.getMax()) {
				wt_past_3_max = lastMonth.getMax();
			}
			i++;
		}

		WeatherMonthParam lastMonth = monthParamMap.get(month.minus(1).getValue());
		wt_past_1_min = lastMonth.getMin();
		wt_past_1_max = lastMonth.getMax();

		whParam.setWt_12_min(wt_12_min);
		whParam.setWt_12_max(wt_12_max);

		whParam.setWt_past_3_min(wt_past_3_min);
		whParam.setWt_past_3_max(wt_past_3_max);

		whParam.setWt_past_1_min(wt_past_1_min);
		whParam.setWt_past_1_max(wt_past_1_max);

		if (wt_12_snow != null) {
			whParam.setWt_12_snow(wt_12_snow);
		} else {
			whParam.setWt_12_snow(false);
		}

		WeatherMonthParam travelMonthParam = monthParamMap.get(month.getValue());
		whParam.setWt_12_snow(travelMonthParam.getRain());

		return whParam;
	}

	/**
	 * Extract weather month params.
	 *
	 * @param city
	 *            the city
	 * @return the map
	 */
	private Map<Integer, WeatherMonthParam> extractWeatherMonthParams(CityEsBean city) {
		Map<String, Map<String, WeatherTypeParam>> weatherRef = city.getWeather().getWeather();
		Map<Integer, WeatherMonthParam> monthParamMap = new HashMap<>();
		for (Map.Entry<String, Map<String, WeatherTypeParam>> monthEntity : weatherRef.entrySet()) {
			Map<String, WeatherTypeParam> monthDetail = monthEntity.getValue();
			WeatherTypeParam minValue = monthDetail.get("Cold nights");
			WeatherTypeParam maxValue = monthDetail.get("Mean daily maximum");
			WeatherTypeParam snowValue = monthDetail.get("Snow days");
			WeatherTypeParam dryDays = monthDetail.get("Dry days");

			boolean snowPossible = false;
			boolean rainPossible = false;
			if (snowValue.getValue() != null && (double) snowValue.getValue() > 0) {
				snowPossible = true;
			}

			if (dryDays.getValue() != null && (double) dryDays.getValue() > 0 && (double) dryDays.getValue() < 15) {
				rainPossible = true;
			}

			Month month = Month.valueOf(monthEntity.getKey().toUpperCase());
			WeatherMonthParam monthParam = new WeatherMonthParam((Integer) minValue.getValue(),
					(Integer) maxValue.getValue(), snowPossible, rainPossible, month.getValue());
			monthParamMap.put(month.getValue(), monthParam);
		}
		return monthParamMap;
	}

	/**
	 * Sets the language.
	 *
	 * @param fromCity
	 *            the from city
	 * @param toCity
	 *            the to city
	 * @param response
	 *            the response
	 */
	private void setLanguage(final CityEsBean fromCity, CityEsBean toCity, TravelCityDetails fromCityDetails,
			TravelCityDetails toCityDetails) {
		Languages toLang = toCity.getLanguagesRef();
		if (toLang != null) {
			if (CollectionUtils.isNotEmpty(toLang.getLanguages())) {
				toCityDetails.setLanguage(getLanguage(toLang.getLanguages()).getLanguage());
			}
		}

		Languages fromLang = fromCity.getLanguagesRef();
		if (fromLang != null) {
			if (CollectionUtils.isNotEmpty(fromLang.getLanguages())) {
				fromCityDetails.setLanguage(getLanguage(fromLang.getLanguages()).getLanguage());
			}
		}
	}

	private LanguageDetails getLanguage(List<LanguageDetails> languages) {
		LanguageDetails langDetail = new LanguageDetails();

		LanguageDetails first = languages.get(0);
		LanguageDetails second = languages.get(1);

		if (first.getLanguage().equalsIgnoreCase("English")) {
			if (second.getRatio() * 100 > 80) {
				langDetail = second;
			} else {
				langDetail = first;
			}

		} else {
			langDetail = first;
		}
		return langDetail;
	}

	/**
	 * Sets the from city fields.
	 *
	 * @param travelMonth
	 *            the travel month
	 * @param fromCity
	 *            the from city
	 * @param response
	 *            the response
	 */
	private void setFromCityFields(final String travelMonth, final CityEsBean fromCity, TravelCityDetails response) {
		if (fromCity != null) {

			response.setGeoId(fromCity.getGeonameId());
			response.setCityName(fromCity.getAsciiName());
			response.setState(fromCity.getState());
			response.setCountry(fromCity.getCountryName());

			response.setCurrencyCode(fromCity.getCurrencyCode());
			response.setEuropean(fromCity.isEuropean());
			response.setHasBeach(fromCity.isBeach());
			response.setHasMountain(fromCity.isMountain());
			response.setPopulationDensity(fromCity.getPopulationRef().getDensity());

			Map<String, Map<String, WeatherTypeParam>> weatherRef = fromCity.getWeather().getWeather();
			response.setWeather(weatherRef.get(travelMonth));

			response.setVegetation(fromCity.getVegetation().getEvi());

			response.setNetwork(fromCity.getNetwork().getNetwork());
			response.setElevation(fromCity.getElevationRef().getElevation());

			WeatherCityParam fromWhParam = setWeatherData(fromCity, travelMonth, response);
			response.setWeatherCityParam(fromWhParam);

			response.setCabs(fromCity.getCabDetails());

			response.setHasSubway(fromCity.getGoogleSubway());
			response.setCityThreat(fromCity.getThreatRatio());
			response.setNetworkOperator(fromCity.getNetworkOperator());
		}
	}

	/**
	 * Sets the to city fields.
	 *
	 * @param travelMonth
	 *            the travel month
	 * @param toCity
	 *            the to city
	 * @param response
	 *            the response
	 */
	private void setToCityFields(final String travelMonth, final CityEsBean toCity, TravelCityDetails response) {
		if (toCity != null) {
			response.setGeoId(toCity.getGeonameId());
			response.setCityName(toCity.getAsciiName());
			response.setState(toCity.getState());
			response.setCountry(toCity.getCountryName());

			response.setCurrencyCode(toCity.getCurrencyCode());
			response.setEuropean(toCity.isEuropean());
			response.setHasBeach(toCity.isBeach());
			response.setHasMountain(toCity.isMountain());
			response.setPopulationDensity(toCity.getPopulationRef().getDensity());

			Map<String, Map<String, WeatherTypeParam>> weatherRef = toCity.getWeather().getWeather();
			response.setWeather(weatherRef.get(travelMonth));

			response.setVegetation(toCity.getVegetation().getEvi());
			response.setNetwork(toCity.getNetwork().getNetwork());
			response.setElevation(toCity.getElevationRef().getElevation());

			WeatherCityParam toWhParam = setWeatherData(toCity, travelMonth, response);
			response.setWeatherCityParam(toWhParam);

			response.setCabs(toCity.getCabDetails());

			response.setHasSubway(toCity.getGoogleSubway());
			response.setCityThreat(toCity.getThreatRatio());
			response.setNetworkOperator(toCity.getNetworkOperator());
		}
	}

	/**
	 * Sets the airport.
	 *
	 * @param fromCity
	 *            the from city
	 * @param toCity
	 *            the to city
	 * @param fromCitydetails
	 *            the from citydetails
	 * @param toCityDetails
	 *            the to city details
	 */
	private void setAirport(CityEsBean fromCity, CityEsBean toCity, TravelCityDetails fromCitydetails,
			TravelCityDetails toCityDetails) {

		boolean hasSameCountry = fromCitydetails.getCountry().equalsIgnoreCase(toCityDetails.getCountry());

		List<AirportDTO> fromCityAirport = getAirport(fromCity, hasSameCountry);
		fromCitydetails.setAirport(fromCityAirport);

		List<AirportDTO> toCityAirport = getAirport(toCity, hasSameCountry);
		toCityDetails.setAirport(toCityAirport);
	}

	/**
	 * Sets the airport.
	 *
	 * @param city
	 *            the to city
	 * @param response
	 *            the response
	 * @return the list
	 */
	private List<AirportDTO> getAirport(CityEsBean city, boolean hasSameCountry) {
		List<Airport> airportList = city.getAirports();
		if (hasSameCountry) {
			Collections.sort(airportList,
					(airport1, airport2) -> airport2.getDomesticCount().compareTo(airport1.getDomesticCount()));
		} else {
			Collections.sort(airportList, (airport1, airport2) -> airport2.getInternationalCount()
					.compareTo(airport1.getInternationalCount()));
		}
		return airportList.stream().map(airport -> EntityToDTO.getInstance().getAiportDTO(airport))
				.collect(Collectors.toList());
	}

	/**
	 * Sets the nearest city.
	 *
	 * @param toCity
	 *            the to city
	 * @param response
	 *            the response
	 * @throws Exception
	 *             the exception
	 */
	private void setNearestCity(CityEsBean toCity, TravelCityDetails response) throws Exception {
		String geoPoint = toCity.getGeoLocation().getLat() + "," + toCity.getGeoLocation().getLon();
		List<CityEsBean> nearCity = geoDataSearchDao.searchCityByGeoPoint(geoPoint, "50");
		List<String> cities = new ArrayList<>();
		for (CityEsBean city : nearCity) {
			cities.add(city.getAsciiName());
		}
		response.setNearestBigCity(cities);
	}

	/**
	 * Sets the holidays.
	 *
	 * @param fromCity
	 *            the from city
	 * @param toCity
	 *            the to city
	 * @param request
	 *            the request
	 * @param fromCityDetails
	 *            the from city details
	 * @param toCityDetails
	 *            the to city details
	 */
	private void setHolidays(CityEsBean fromCity, CityEsBean toCity, TravelRequest request,
			TravelCityDetails fromCityDetails, TravelCityDetails toCityDetails) {
		try {
			if (fromCity != null) {
				setLongWeekendStats(fromCity, request, fromCityDetails, true);
			}
			if (toCity != null) {
				setLongWeekendStats(toCity, request, toCityDetails, false);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Process days.
	 *
	 * @param request
	 *            the request
	 * @param nonWorkingdays
	 *            the non workingdays
	 * @param holidayList
	 *            the holiday list
	 * @return the travel days stats
	 */
	private Map<String, Boolean> getTravelDaysStats(TravelRequest request, List<DayOfWeek> nonWorkingdays,
			List<Holiday> holidayList) {
		Map<String, Boolean> daysMap = new LinkedHashMap<>();
		try {
			String travelDate = request.getStartDate();
			LocalDate travelLocalDate = WooDateUtils.getDate(travelDate);
			for (int i = 0; i <= request.getDuration(); i++) {
				LocalDate compareDate = travelLocalDate.plusDays(i);
				if (nonWorkingdays.contains(compareDate.getDayOfWeek())) {
					// Non-Working Days
					daysMap.put(WooDateUtils.formatDate(compareDate), false);
				} else {
					// Working Days
					daysMap.put(WooDateUtils.formatDate(compareDate), true);
				}
			}

			List<String> travelholiday = new ArrayList<>();
			Iterator<Map.Entry<String, Boolean>> localDateItertor = daysMap.entrySet().iterator();
			while (localDateItertor.hasNext()) {
				Map.Entry<String, Boolean> entry = localDateItertor.next();
				if (entry.getValue()) {
					for (Holiday holiday : holidayList) {
						LocalDate holidayDate = WooDateUtils.getDate(holiday.getHolidayDate());
						if (holidayDate.isEqual(WooDateUtils.getDate(entry.getKey()))) {
							travelholiday.add(entry.getKey());
						}
					}
				}
			}
			travelholiday.stream().forEach(holiday -> {
				daysMap.put(holiday, false);
			});

		} catch (Exception e) {
			e.printStackTrace();
		}
		return daysMap;
	}

	/**
	 * Process days.
	 *
	 * @param request
	 *            the request
	 * @param nonWorkingdays
	 *            the non workingdays
	 * @param holidayList
	 *            the holiday list
	 * @return the immutable pair
	 */
	private ImmutablePair<List<LocalDate>, List<LocalDate>> processDays(TravelRequest request,
			List<DayOfWeek> nonWorkingdays, List<Holiday> holidayList) {
		List<LocalDate> workingDayList = new ArrayList<>();
		List<LocalDate> nonWorkingDayList = new ArrayList<>();
		try {
			String travelDate = request.getStartDate();
			LocalDate travelLocalDate = WooDateUtils.getDate(travelDate);
			for (int i = 0; i <= request.getDuration(); i++) {
				LocalDate compareDate = travelLocalDate.plusDays(i);
				if (nonWorkingdays.contains(compareDate.getDayOfWeek())) {
					nonWorkingDayList.add(compareDate);
				} else {
					workingDayList.add(compareDate);
				}
			}

			List<LocalDate> removeItem = new ArrayList<>();
			Iterator<LocalDate> localDateItertor = workingDayList.iterator();
			while (localDateItertor.hasNext()) {
				LocalDate date = localDateItertor.next();
				for (Holiday holiday : holidayList) {
					LocalDate holidayDate = WooDateUtils.getDate(holiday.getHolidayDate());
					if (holidayDate.isEqual(date)) {
						// localDateItertor.remove();
						nonWorkingDayList.add(date);
						removeItem.add(date);
					}
				}
			}
			workingDayList.remove(removeItem);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ImmutablePair<List<LocalDate>, List<LocalDate>>(workingDayList, nonWorkingDayList);
	}

	/**
	 * Gets the valid holidays.
	 *
	 * @param travelDate
	 *            the travel date
	 * @param days
	 *            the days
	 * @param cityBean
	 *            the city bean
	 * @return the valid holidays
	 */
	private List<Holiday> getValidHolidays(final String travelDate, final Integer days, CityEsBean cityBean) {
		List<Holiday> holidayList = cityBean.getHolidays();
		List<Holiday> validHolidays = new ArrayList<>();
		for (Holiday holiday : holidayList) {
			if (isDateValid(travelDate, days, holiday)) {
				validHolidays.add(holiday);
			}
		}
		return validHolidays;
	}

	/**
	 * Checks if is date valid.
	 *
	 * @param travelDate
	 *            the travel date
	 * @param noOfDays
	 *            the no of days
	 * @param holiday
	 *            the holiday
	 * @return true, if is date valid
	 */
	private boolean isDateValid(final String travelDate, final Integer noOfDays, Holiday holiday) {
		boolean dateValid = false;
		try {
			LocalDate travelLocalDate = WooDateUtils.getDate(travelDate);
			LocalDate holidayDate = WooDateUtils.getDate(holiday.getHolidayDate());
			if ((travelLocalDate.getYear() == holidayDate.getYear())
					&& (travelLocalDate.getMonth() == holidayDate.getMonth())) {
				dateValid = true;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return dateValid;
	}

	/**
	 * Sets the long weekend.
	 *
	 * @param cityBean
	 *            the city bean
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param fromCity
	 *            the from city
	 */
	private void setLongWeekendStats(CityEsBean cityBean, TravelRequest request, TravelCityDetails cityDetails,
			boolean fromCity) {
		boolean isLongWeekend = false;
		String workingDay = cityBean.getWorkingDays();
		List<Holiday> holidayList = new ArrayList<>();
		List<DayOfWeek> nonWorkingdays = new ArrayList<>();
		if (!"NA".equalsIgnoreCase(workingDay)) {
			nonWorkingdays = DayUtils.getNonWorkingDays(workingDay);
			holidayList = getValidHolidays(request.getStartDate(), request.getDuration(), cityBean);
			ImmutablePair<List<LocalDate>, List<LocalDate>> dayPair = processDays(request, nonWorkingdays, holidayList);
			if (dayPair.getLeft().size() <= dayPair.getRight().size()) {
				isLongWeekend = true;
			}
		} else {
			isLongWeekend = false;
		}
		if (fromCity) {
			cityDetails.setDaysStats(getTravelDaysStats(request, nonWorkingdays, holidayList));
			cityDetails.setCityLongWeekend(isLongWeekend);
		} else {
			cityDetails.setDaysStats(getTravelDaysStats(request, nonWorkingdays, holidayList));
			cityDetails.setCityLongWeekend(isLongWeekend);
		}
	}

}
