package com.woocation.inference;

import lombok.Data;

/**
 * Instantiates a new weather city param.
 */
@Data
public class WeatherCityParam {

	/** The wt 12 min. */
	private Integer wt_12_min;

	/** The wt 12 max. */
	private Integer wt_12_max;

	/** The wt past 3 min. */
	private Integer wt_past_3_min;

	/** The wt past 3 max. */
	private Integer wt_past_3_max;

	/** The wt past 1 min. */
	private Integer wt_past_1_min;

	/** The wt past 1 max. */
	private Integer wt_past_1_max;

	/** The wt 12 snow. */
	private Boolean wt_12_snow;

	/** The wt 3 rain. */
	private Boolean wt_current_rain;
}
