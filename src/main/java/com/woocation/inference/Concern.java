/**
 * 
 */
package com.woocation.inference;

/**
 * The Enum Concern.
 *
 * @author ankit.gupta
 */
public enum Concern {

	NO_CONERN,
	
	/** The very low. */
	VERY_LOW,

	/** The low. */
	LOW,

	/** The medium. */
	MEDIUM,

	/** The high. */
	HIGH,

	/** The critical. */
	CRITICAL

}
