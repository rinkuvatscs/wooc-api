package com.woocation.inference;

import java.util.Arrays;
import java.util.List;

/**
 * The Enum TravelMetas.
 */
public enum TravelMetas {

	/** The traveltype. */
	TRAVELTYPE("Business", "LEasure"), 
	
	/** The food. */
	FOOD("VEG", "NON-VEG"), 
	
	/** The disability. */
	DISABILITY("", ""), 
	
	/** The pets. */
	PETS("", ""), 
	
	/** The religion. */
	RELIGION("HINDU", "MUSLIM");

	private List<String> values;
	
	/**
	 * Instantiates a new travel metas.
	 *
	 * @param metas the metas
	 */
	private TravelMetas(String... metas) {
		this.values = Arrays.asList(metas);
	}

	/**
	 * @return the values
	 */
	public List<String> getValues() {
		return values;
	}

	/**
	 * @param values the values to set
	 */
	public void setValues(List<String> values) {
		this.values = values;
	}
	
}
