package com.woocation.inference;

/**
 * The Class Passenger.
 */
public class Passenger {

	/**
	 * The Enum GENDER.
	 */
	public enum GENDER {

		/** The male. */
		MALE,
		/** The female. */
		FEMALE
	};

	/** The gender. */
	private GENDER gender;

	/** The age. */
	protected Integer age;

	/**
	 * Gets the age.
	 *
	 * @return the age
	 */
	public Integer getAge() {
		return age;
	}

	/**
	 * Sets the age.
	 *
	 * @param age
	 *            the age to set
	 */
	public void setAge(Integer age) {
		this.age = age;
	}

	/**
	 * Gets the gender.
	 *
	 * @return the gender
	 */
	public GENDER getGender() {
		return gender;
	}

	/**
	 * Sets the gender.
	 *
	 * @param gender
	 *            the gender to set
	 */
	public void setGender(GENDER gender) {
		this.gender = gender;
	}
}
