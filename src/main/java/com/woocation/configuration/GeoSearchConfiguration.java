package com.woocation.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import lombok.Data;

/**
 * The Class GeoSearchConfiguration.
 */
@Data
@Configuration
public class GeoSearchConfiguration {

	/** The geo IP search URL. */
	@Value("${geolocation.trace.url}")
	private String geoIPSearchURl;

	/** The geo IP search URL. */
	@Value("${geolocation.latlong.search}")
	private boolean geoPointSearch;

	/** The geo IP search URL. */
	@Value("${goelocation.response.class}")
	private String geoResponseClass;
	
	@Value("${hotel.amenity.search.url}")
	private String hotelAmenityURL;
	
	@Value("${hotel.places.search.url}")
	private String hotelPlacesURL;
}
