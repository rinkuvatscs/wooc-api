package com.woocation.service;

import java.util.List;

import com.woocation.dto.HotelSuggestDTO;
import com.woocation.model.BookingHotel;
import com.woocation.service.request.DataUploadRequest;
import com.woocation.service.response.GeoDataUploadResponse;

/**
 * The Interface HotelUploadService.
 */
public interface HotelUploadService {

	/**
	 * Upload hotel data.
	 *
	 * @param uploadRequest
	 *            the upload request
	 * @return the geo data upload response
	 */
	public GeoDataUploadResponse uploadHotelData(DataUploadRequest uploadRequest);

	/**
	 * Search hotel by geo point.
	 *
	 * @param name
	 *            the name
	 * @param geoPoint
	 *            the geo point
	 * @param distance
	 *            the distance
	 * @return the list
	 */
	public List<BookingHotel> searchHotelByGeoPoint(final String name, String geoPoint, String distance);

	/**
	 * Suggest.
	 *
	 * @param hotelText
	 *            the hotel text
	 * @return the hotel suggest dto
	 */
	public List<HotelSuggestDTO> suggest(String hotelText);

}
