package com.woocation.service.impl;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.elasticsearch.search.suggest.completion.CompletionSuggestion;
import org.elasticsearch.search.suggest.completion.CompletionSuggestion.Entry.Option;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParser.Feature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.woocation.configuration.MasterDataConfig;
import com.woocation.dao.HotelSearchDao;
import com.woocation.dto.HotelSuggestDTO;
import com.woocation.elastic.core.ElasticEntityManager;
import com.woocation.model.BookingHotel;
import com.woocation.service.HotelUploadService;
import com.woocation.service.request.DataUploadRequest;
import com.woocation.service.response.GeoDataUploadResponse;

/**
 * The Class GeoDataUploadServiceImpl.
 */
@Service
public class HotelUploadServiceImpl implements HotelUploadService {

	/** The logger. */
	private static final Logger logger = LoggerFactory.getLogger(HotelUploadServiceImpl.class);

	/** The geo config. */
	@Autowired
	private MasterDataConfig masterConfig;

	@Autowired
	private HotelSearchDao hotelSearchDao;

	/** The entity manager. */
	@Autowired
	private ElasticEntityManager entityManager;

	@Override
	public GeoDataUploadResponse uploadHotelData(DataUploadRequest uploadRequest) {
		GeoDataUploadResponse response = new GeoDataUploadResponse();
		if (uploadRequest.getDataType().equalsIgnoreCase("Hotel")) {
			try {
				uploadHotelData(uploadRequest.getFileLocation(), masterConfig.getHotelIndexName(),
						masterConfig.getHotelDocType());
				response.setStatusMessage("Data uploaded success");
				response.setStatus(true);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {

		}
		return response;
	}

	@Override
	public List<BookingHotel> searchHotelByGeoPoint(String name, String geoPoint, String distance) {
		List<BookingHotel> hotelList = new ArrayList<>();
		try {
			hotelList = hotelSearchDao.searchHotelByGeoPoint(name, geoPoint, distance);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return hotelList;
	}

	@Override
	public List<HotelSuggestDTO> suggest(String hotelText) {
		List<HotelSuggestDTO> suggestList = new ArrayList<>();
		try {
//			SearchResponse searchResponse = hotelSearchDao.hotelSuggest(hotelText);
//			CompletionSuggestion hotelSuggestion = searchResponse.getSuggest().getSuggestion("hotelSuggest");
//			suggestList = extractSuggestResults(hotelSuggestion);
			List<BookingHotel> hotelList = hotelSearchDao.hotelSuggest(hotelText);
			if(CollectionUtils.isNotEmpty(hotelList)){
				for(BookingHotel hotel : hotelList){
					HotelSuggestDTO suggestDto = new HotelSuggestDTO();
					BeanUtils.copyProperties(hotel, suggestDto);
					String fullName = suggestDto.getName() + ", " + suggestDto.getCity() + ", " + suggestDto.getCountry();
					suggestDto.setSuggestKey(fullName);
					suggestList.add(suggestDto);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return suggestList;
	}

	/**
	 * Extract suggest results.
	 *
	 * @param skilCompletionSuggestion
	 *            the skil completion suggestion
	 * @return the list
	 * @throws Exception
	 *             the exception
	 */
	private List<HotelSuggestDTO> extractSuggestResults(CompletionSuggestion skilCompletionSuggestion)
			throws Exception {
		List<CompletionSuggestion.Entry> entries = skilCompletionSuggestion.getEntries();
		List<HotelSuggestDTO> suggestList = new ArrayList<>();
		if (!entries.isEmpty()) {
			CompletionSuggestion.Entry entry = entries.get(0);
			List<Option> options = entry.getOptions();
			for (Option option : options) {
				String key = option.getText().toString();
				Map<String, Object> hotel = option.getHit().getSourceAsMap();
				suggestList.add(getHotelSuggestBean(hotel, key));
			}
		}
		return suggestList;
	}

	/**
	 * Gets the hotel suggest bean.
	 *
	 * @param hotel
	 *            the hotel
	 * @param searchMatchedKey
	 *            the search matched key
	 * @return the hotel suggest bean
	 * @throws Exception
	 *             the exception
	 */
	private HotelSuggestDTO getHotelSuggestBean(Map<String, Object> hotel, String searchMatchedKey) throws Exception {
		Object hotelId = hotel.get("hotelId");
		String hotelName = hotel.get("name").toString();
		String city = hotel.get("city").toString();
		String countryName = hotel.get("country").toString();
		Object location = hotel.get("location");

		HotelSuggestDTO suggestDto = new HotelSuggestDTO();

		suggestDto.setHotelId(hotelId);
		suggestDto.setName(hotelName);
		suggestDto.setCity(city);
		suggestDto.setCountry(countryName);
		String fullName = hotelName + ", " + city + ", " + countryName;
		suggestDto.setSuggestKey(fullName);
		suggestDto.setLocation(location);
		return suggestDto;
	}

	/**
	 * Upload geo data.
	 *
	 * @param path
	 *            the path
	 * @param indexName
	 *            the index name
	 * @param docType
	 *            the doc type
	 * @return true, if successful
	 * @throws Exception
	 *             the exception
	 */
	private boolean uploadHotelData(final String path, final String indexName, final String docType) throws Exception {
		boolean uploadStatus = false;
		try {
			if (entityManager.validateIndex(indexName, docType, BookingHotel.class)) {
				List<File> filesInFolder = Files.walk(Paths.get(path)).filter(Files::isRegularFile).map(Path::toFile)
						.collect(Collectors.toList());
				for (File file : filesInFolder) {
					List<BookingHotel> hotelList = readHotelData(file.getAbsolutePath());
					hotelList.stream().forEach(hotel -> hotel.setHotelSuggest());
					if (hotelList != null && hotelList.size() > 0) {
						entityManager.saveAll(hotelList, indexName, docType);
					}
				}
				uploadStatus = true;
			}
		} catch (Exception e) {
			throw new Exception(e);
		}
		return uploadStatus;
	}

	/**
	 * Read subway file.
	 */
	private List<BookingHotel> readHotelData(String filePath) {
		try {
			return readFile(filePath, BookingHotel.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Read file.
	 *
	 * @param <T> the generic type
	 * @param fileName the file name
	 * @param pojoClass the pojo class
	 * @return the list
	 */
	private <T> List<T> readFile(String fileName, Class<T> pojoClass) {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(Feature.ALLOW_NON_NUMERIC_NUMBERS, true);
		List<T> list = null;
		try {
			File jsonFile = new File(fileName);
			list = mapper.readValue(jsonFile, mapper.getTypeFactory().constructCollectionType(List.class, pojoClass));
			System.out.println("Records Read in File --> " + fileName + " --> " + list.size());

		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;

	}

}
