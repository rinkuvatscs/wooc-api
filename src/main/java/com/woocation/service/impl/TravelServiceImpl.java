package com.woocation.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.google.common.base.Joiner;
import com.woocation.configuration.GeoSearchConfiguration;
import com.woocation.dao.GeoDataSearchDao;
import com.woocation.dao.HotelSearchDao;
import com.woocation.elastic.core.EntityMapper;
import com.woocation.inference.TravelType;
import com.woocation.inference.TravellerType;
import com.woocation.inference.extracter.CityDetailsExtracter;
import com.woocation.inference.extracter.InferenceExtracter;
import com.woocation.inference.response.InferenceParam;
import com.woocation.inference.response.InferenceResponse;
import com.woocation.inference.response.TravelCityParam;
import com.woocation.model.AmenityDTO;
import com.woocation.model.BookingHotel;
import com.woocation.model.CityEsBean;
import com.woocation.service.GeoDataSearchService;
import com.woocation.service.TravelService;
import com.woocation.service.request.TravelRequest;
import com.woocation.service.response.GeoIPLocationResponse;
import com.woocation.service.response.Response;
import com.woocation.util.GeoIPExtractor;
import com.woocation.util.WooDateUtils;

/**
 * The Class TravelServiceImpl.
 */
@Service
public class TravelServiceImpl implements TravelService {

	/** The logger. */
	private static final Logger logger = LoggerFactory.getLogger(TravelServiceImpl.class);

	/** The geo data search dao. */
	@Autowired
	private GeoDataSearchDao geoDataSearchDao;

	@Autowired
	private CityDetailsExtracter cityDetailsExtracter;

	@Autowired
	private InferenceExtracter inferenceExtracter;
	
	/** The hotel search dao. */
	@Autowired
	private HotelSearchDao hotelSearchDao;

	/** The geo data search service. */
	@Autowired
	private GeoDataSearchService geoDataSearchService;

	/** The geo config. */
	@Autowired
	private GeoSearchConfiguration geoConfig;
	
	/** The rest template. */
	@Autowired
	private RestTemplate restTemplate;

	@Override
	public InferenceResponse getTravelDetails(TravelRequest travelRequest) throws Exception {
		InferenceResponse inferenceResponse = new InferenceResponse();

		long startTimeTags = System.currentTimeMillis();
		
		if (WooDateUtils.validateDate(travelRequest.getStartDate())) {
			getInferenceTags(travelRequest, inferenceResponse);
		}
		long endTimeTag = System.currentTimeMillis();
		logger.debug("Time take in Fetching tags --> " + (endTimeTag - startTimeTags));

		if (inferenceResponse.getTravelDetailsResponse() != null && inferenceResponse.getInferenceParam() != null) {
			fetchHotelAmenity(inferenceResponse);
			long endHotelAmenity = System.currentTimeMillis();
			logger.debug("Time take in Fetching Hotel Amenity --> " + (endHotelAmenity - endTimeTag));
			
			fetchHotelPlaces(inferenceResponse);
			
			logger.debug("Time take in Fetching Hotel Places --> " + (System.currentTimeMillis() - endHotelAmenity));
		}
		return inferenceResponse;
	}

	
	/**
	 * Fetch hotel places.
	 *
	 * @param inferenceResponse the inference response
	 */
	private void fetchHotelPlaces(InferenceResponse inferenceResponse){
		try {
			BookingHotel hotel = inferenceResponse.getHotel();
			String placeSearchUrl = geoConfig.getHotelPlacesURL() + "?lat=" + hotel.getLocation().getLat() + "&lon="
					+ hotel.getLocation().getLon();
			Object output = restTemplate.getForObject(placeSearchUrl, Object.class);
			inferenceResponse.setLocalPlaces(output);
		} catch (RestClientException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Fetch hotel amenity.
	 *
	 * @param inferenceResponse
	 *            the inference response
	 */
	private void fetchHotelAmenity(InferenceResponse inferenceResponse) {
		try {
			InferenceParam inferenceParam = inferenceResponse.getInferenceParam();

			Long hotelId = inferenceResponse.getHotel().getHotelId();
			List<String> inferenceTags = getInferenceTags(inferenceParam);
			inferenceParam.setInferenceTags(inferenceTags);

//			hotelId = 2097187L;
			String inferenceUrls = getUrl(hotelId, inferenceTags);
			RestTemplate restTemplate = new RestTemplate();
			String output = restTemplate.getForObject(inferenceUrls, String.class);
			@SuppressWarnings("unchecked")
			Response<AmenityDTO> response = EntityMapper.getInstance().getObject(output, Response.class);

			if (response != null && CollectionUtils.isNotEmpty(response.getResults())) {
				inferenceResponse.setHotelAmenity(response.getResults());
			}
		} catch (Exception e) {
			logger.error("Error while fetching Hotel Amenity " + e.getMessage());
		}
	}

	/**
	 * Gets the inference tags.
	 *
	 * @param param
	 *            the param
	 * @return the inference tags
	 */
	private List<String> getInferenceTags(InferenceParam param) {
		List<String> tagList = new ArrayList<>();

		if (param.getGoingToCold() != null && param.getGoingToCold()) {
			tagList.add("cold");
		}
		if (param.getGoingToHot() != null && param.getGoingToHot()) {
			tagList.add("hot");
		}

		if (param.getGoingToMountain() != null && param.getGoingToMountain()) {
			tagList.add("mountain");
		}

		if (param.getSnowPossible() != null && param.getSnowPossible()) {
			tagList.add("snow");
		}
		
		if (param.getTravellerType() != null) {
			TravellerType travellerType = param.getTravellerType(); 
			tagList.addAll(travellerType.getTravellerTags());
		}
		
		if (param.getTravelType() != null) {
			TravelType travelType = param.getTravelType(); 
			tagList.addAll(travelType.getTravelTags());
		}
		
		if(param.isInternational()){
			tagList.add("international");
		}
		
		String connectityConcern = param.getConnectivityConcern();
		if(StringUtils.isNotEmpty(connectityConcern) && (connectityConcern.equalsIgnoreCase("High") || connectityConcern.equalsIgnoreCase("Critical"))){
			tagList.add("connectivity");
		}
		return tagList;
	}

	/**
	 * Gets the inference tags.
	 *
	 * @param travelRequest
	 *            the travel request
	 * @param inferenceResponse
	 *            the inference response
	 * @return the inference tags
	 * @throws Exception
	 *             the exception
	 */
	private void getInferenceTags(TravelRequest travelRequest, InferenceResponse inferenceResponse) throws Exception {
		TravelCityParam cityParam;
		ImmutablePair<CityEsBean, CityEsBean> cityList = getCityBean(travelRequest);
		cityParam = cityDetailsExtracter.extractTravelCityDetails(travelRequest, cityList.getLeft(),
				cityList.getRight());
		setHotelName(travelRequest, cityParam, inferenceResponse);
		InferenceParam inferenceTags = inferenceExtracter.extractInferenceDetails(travelRequest, cityParam,
				cityList.getLeft(), cityList.getRight());
		inferenceResponse.setInferenceParam(inferenceTags);
		inferenceResponse.setTravelDetailsResponse(cityParam);
	}

	/**
	 * Gets the city bean.
	 *
	 * @param travelRequest
	 *            the travel request
	 * @return the city bean
	 */
	private ImmutablePair<CityEsBean, CityEsBean> getCityBean(TravelRequest travelRequest) {
		Long fromCityGeoId = null;
		CityEsBean fromCity = null;
		CityEsBean toCity = null;

		try {
			Integer duration = WooDateUtils.getDuration(travelRequest.getStartDate(), travelRequest.getEndDate());
			travelRequest.setDuration(duration);

			if (travelRequest.getFromGeoNameId() == null) {
				fromCityGeoId = getFromLocation(travelRequest);
				travelRequest.setFromGeoNameId(fromCityGeoId);
			}

			if (travelRequest.getToGeoNameId() == null) {
				if (StringUtils.isNotEmpty(travelRequest.getHotelLocation())) {
					List<CityEsBean> toCityList = geoDataSearchDao
							.searchCityByGeoPoint(travelRequest.getHotelLocation(), "50km");
					if (CollectionUtils.isNotEmpty(toCityList)) {
						travelRequest.setToGeoNameId(toCityList.get(0).getGeonameId());
					}
				} 
			}
			if (travelRequest.getFromGeoNameId() != null) {
				fromCity = geoDataSearchDao.getCity(travelRequest.getFromGeoNameId());
			}
			if (travelRequest.getToGeoNameId() != null) {
				toCity = geoDataSearchDao.getCity(travelRequest.getToGeoNameId());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ImmutablePair<CityEsBean, CityEsBean>(fromCity, toCity);
	}

	/**
	 * Sets the hotel name.
	 *
	 * @param travelRequest
	 *            the travel request
	 * @param response
	 *            the response
	 */
	private void setHotelName(TravelRequest travelRequest, TravelCityParam cityParam, InferenceResponse response) {
		if (StringUtils.isNotBlank(travelRequest.getHotelLocation())) {
			try {
				List<BookingHotel> hotelList = hotelSearchDao.searchHotelByGeoPoint(travelRequest.getHotelName(),
						travelRequest.getHotelLocation(), "20");
				if (CollectionUtils.isNotEmpty(hotelList)) {
					response.setHotel(hotelList.get(0));
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Gets the from location.
	 *
	 * @param ipAddress
	 *            the ip address
	 * @param location
	 *            the location
	 * @return the from location
	 */
	private Long getFromLocation(TravelRequest travelRequest) {
		Long geoNameId = null;
		try {
			if (StringUtils.isNotEmpty(travelRequest.getFromLocation())) {
				List<CityEsBean> esBeanList = geoDataSearchDao.searchCityByGeoPoint(travelRequest.getFromLocation(),
						"50");
				if (esBeanList != null && esBeanList.size() > 0) {
					geoNameId = esBeanList.get(0).getGeonameId();
				}
			} else if (StringUtils.isNotEmpty(travelRequest.getFromIPAddress())) {
				GeoIPLocationResponse geoResponse = geoDataSearchService
						.searchCityByIP(travelRequest.getFromIPAddress());
				if (geoResponse != null) {
					geoNameId = geoResponse.getGeoNameId();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return geoNameId;
	}

	/**
	 * Gets the url.
	 *
	 * @param hotelId
	 *            the hotel id
	 * @param tags
	 *            the tags
	 * @return the url
	 */
	private String getUrl(Long hotelId, List<String> tags) {
		StringBuilder urlBuilder = new StringBuilder();
		urlBuilder.append(geoConfig.getHotelAmenityURL());
		urlBuilder.append("?hotelId=");
		urlBuilder.append(hotelId);
		urlBuilder.append("&persona=");
		urlBuilder.append(Joiner.on(",").join(tags));
		return urlBuilder.toString();
	}

}
