package com.woocation.service.response;

/**
 * The Class FreeGeoIPResponse.
 */
public class FreeGeoIPResponse {

	/** The time zone. */
	private String time_zone;

	/** The region name. */
	private String region_name;

	/** The metro code. */
	private String metro_code;

	/** The zip code. */
	private String zip_code;

	/** The region code. */
	private String region_code;

	/** The longitude. */
	private String longitude;

	/** The latitude. */
	private String latitude;

	/** The country code. */
	private String country_code;

	/** The country name. */
	private String country_name;

	/** The city. */
	private String city;

	/** The ip. */
	private String ip;
	
	private Long geoNameId;

	/**
	 * Gets the time zone.
	 *
	 * @return the time zone
	 */
	public String getTime_zone() {
		return time_zone;
	}

	/**
	 * Sets the time zone.
	 *
	 * @param time_zone
	 *            the new time zone
	 */
	public void setTime_zone(String time_zone) {
		this.time_zone = time_zone;
	}

	/**
	 * Gets the region name.
	 *
	 * @return the region name
	 */
	public String getRegion_name() {
		return region_name;
	}

	/**
	 * Sets the region name.
	 *
	 * @param region_name
	 *            the new region name
	 */
	public void setRegion_name(String region_name) {
		this.region_name = region_name;
	}

	/**
	 * Gets the metro code.
	 *
	 * @return the metro code
	 */
	public String getMetro_code() {
		return metro_code;
	}

	/**
	 * Sets the metro code.
	 *
	 * @param metro_code
	 *            the new metro code
	 */
	public void setMetro_code(String metro_code) {
		this.metro_code = metro_code;
	}

	/**
	 * Gets the zip code.
	 *
	 * @return the zip code
	 */
	public String getZip_code() {
		return zip_code;
	}

	/**
	 * Sets the zip code.
	 *
	 * @param zip_code
	 *            the new zip code
	 */
	public void setZip_code(String zip_code) {
		this.zip_code = zip_code;
	}

	/**
	 * Gets the region code.
	 *
	 * @return the region code
	 */
	public String getRegion_code() {
		return region_code;
	}

	/**
	 * Sets the region code.
	 *
	 * @param region_code
	 *            the new region code
	 */
	public void setRegion_code(String region_code) {
		this.region_code = region_code;
	}

	/**
	 * Gets the longitude.
	 *
	 * @return the longitude
	 */
	public String getLongitude() {
		return longitude;
	}

	/**
	 * Sets the longitude.
	 *
	 * @param longitude
	 *            the new longitude
	 */
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	/**
	 * Gets the latitude.
	 *
	 * @return the latitude
	 */
	public String getLatitude() {
		return latitude;
	}

	/**
	 * Sets the latitude.
	 *
	 * @param latitude
	 *            the new latitude
	 */
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	/**
	 * Gets the country code.
	 *
	 * @return the country code
	 */
	public String getCountry_code() {
		return country_code;
	}

	/**
	 * Sets the country code.
	 *
	 * @param country_code
	 *            the new country code
	 */
	public void setCountry_code(String country_code) {
		this.country_code = country_code;
	}

	/**
	 * Gets the country name.
	 *
	 * @return the country name
	 */
	public String getCountry_name() {
		return country_name;
	}

	/**
	 * Sets the country name.
	 *
	 * @param country_name
	 *            the new country name
	 */
	public void setCountry_name(String country_name) {
		this.country_name = country_name;
	}

	/**
	 * Gets the city.
	 *
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * Sets the city.
	 *
	 * @param city
	 *            the new city
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * Gets the ip.
	 *
	 * @return the ip
	 */
	public String getIp() {
		return ip;
	}

	/**
	 * Sets the ip.
	 *
	 * @param ip
	 *            the new ip
	 */
	public void setIp(String ip) {
		this.ip = ip;
	}
	

	/**
	 * @return the geoNameId
	 */
	public Long getGeoNameId() {
		return geoNameId;
	}

	/**
	 * @param geoNameId the geoNameId to set
	 */
	public void setGeoNameId(Long geoNameId) {
		this.geoNameId = geoNameId;
	}

	@Override
	public String toString() {
		return "FreeGeoIP --> [time_zone = " + time_zone + ", region_name = " + region_name + ", metro_code = " + metro_code
				+ ", zip_code = " + zip_code + ", region_code = " + region_code + ", longitude = " + longitude
				+ ", latitude = " + latitude + ", country_code = " + country_code + ", country_name = " + country_name
				+ ", city = " + city + ", ip = " + ip + "]";
	}
}