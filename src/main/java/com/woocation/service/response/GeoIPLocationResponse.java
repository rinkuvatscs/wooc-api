package com.woocation.service.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

/**
 * The Class GeoIPLocationResponse.
 */
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class GeoIPLocationResponse {

	/** The country code. */
	private String country_code;

	/** The country name. */
	private String country_name;

	/** The city. */
	private String city;

	/** The postal. */
	private String postal;

	/** The latitude. */
	private String latitude;

	/** The longitude. */
	private String longitude;

	/** The I pv 4. */
	private String IPv4;

	/** The state. */
	private String state;
	
	/** The geo name id. */
	private Long geoNameId;

	

}
