package com.woocation.service;

import com.woocation.service.request.DataUploadRequest;
import com.woocation.service.response.GeoDataUploadResponse;

public interface GeoDataUploadService {
	
	public GeoDataUploadResponse uploadGeoData(DataUploadRequest uploadRequest);

}
