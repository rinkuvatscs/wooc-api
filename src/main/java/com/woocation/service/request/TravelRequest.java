package com.woocation.service.request;

import javax.validation.constraints.NotNull;

import lombok.Data;

/**
 * Instantiates a new travel request.
 */
@Data
public class TravelRequest {

	/** The from geo name id. */
	private Long fromGeoNameId;

	/** The to geo name id. */
	private Long toGeoNameId;

	/** The date. */
	private String bookingDate;

	/** The travel date. */
	@NotNull
	private String startDate;
	
	/** The end date. */
	@NotNull
	private String endDate;
	
	/** The duration. */
	private Integer duration;
	
	/** The adult count. */
	private Integer adultCount;
	
	/** The child count. */
	private Integer childCount;
	
	/** The child age. */
	private Integer childAge;

	/** The location. */
	private String fromLocation;

	/** The from IP address. */
	private String fromIPAddress;

	/** The to IP address. */
	private String toIPAddress;

	/** The hotel location. */
	@NotNull
	private String hotelLocation;

	/** The hotel name. */
	private String hotelName;
	
	/** The relation. */
	private String relation;

}
