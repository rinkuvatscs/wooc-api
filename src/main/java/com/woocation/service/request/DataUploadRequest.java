package com.woocation.service.request;

import lombok.Data;

/**
 * The Class GeoDataUploadRequest.
 *
 * @author ankit
 */
@Data
public class DataUploadRequest {

	/** The geo data type. */
	private String dataType;

	/** The file location. */
	private String fileLocation;
	
}
