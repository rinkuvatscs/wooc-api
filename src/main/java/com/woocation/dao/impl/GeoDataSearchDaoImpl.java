package com.woocation.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.google.common.collect.Lists;
import com.woocation.configuration.GeoSearchConfiguration;
import com.woocation.configuration.MasterDataConfig;
import com.woocation.configuration.SearchFieldConfig;
import com.woocation.dao.GeoDataSearchDao;
import com.woocation.elastic.bean.EsSuggest;
import com.woocation.elastic.builder.CompletionBuilder;
import com.woocation.elastic.builder.EsQuery;
import com.woocation.elastic.builder.EsQuery.EsQueryIntMode;
import com.woocation.elastic.builder.QueryBuilderHelper;
import com.woocation.elastic.builder.WooAggregationBuilderHelper;
import com.woocation.elastic.core.ElasticEntityManager;
import com.woocation.elastic.core.EntityMapper;
import com.woocation.elastic.enums.EsModeQuery;
import com.woocation.exception.ElasticException;
import com.woocation.model.CityEsBean;

@Repository
public class GeoDataSearchDaoImpl implements GeoDataSearchDao {

	/** The geo config. */
	@Autowired
	private MasterDataConfig masterConfig;

	@Autowired
	private SearchFieldConfig fieldConfig;

	@Autowired
	private GeoSearchConfiguration geoConfig;

	/** The entity manager. */
	@Autowired
	private ElasticEntityManager entityManager;

	public GeoDataSearchDaoImpl() {

	}

	@Override
	public CityEsBean getCity(Long geoNameId) throws Exception {
		CityEsBean cityBean = null;
		List<EsQuery> geoSearchQuery = QueryBuilderHelper.getSearchQuery(Lists.newArrayList(geoNameId),
				Lists.newArrayList(fieldConfig.getGeoNameId()));

		QueryBuilder searchQueryBuilder = QueryBuilderHelper.search(geoSearchQuery, EsQueryIntMode.MUST);
		SearchResponse response = entityManager.executeQuery(searchQueryBuilder, null, 0, 20,
				masterConfig.getGeoCityIndexName());
		if (response != null && response.getHits().getTotalHits() == 1) {
			cityBean = EntityMapper.getInstance().getObject(response.getHits().getHits()[0].getSourceAsString(),
					CityEsBean.class);
		}
		return cityBean;
	}

	@Override
	public SearchResponse cityByName(String asciiName) throws ElasticException {
		List<EsQuery> geoSearchQuery = QueryBuilderHelper.getSearchQuery(Lists.newArrayList(asciiName.toLowerCase()),
				Lists.newArrayList(fieldConfig.getGeoAsciiName()));

		QueryBuilder searchQueryBuilder = QueryBuilderHelper.search(geoSearchQuery, EsQueryIntMode.MUST);
		return entityManager.executeQuery(searchQueryBuilder, null, 0, 20, masterConfig.getGeoCityIndexName(),
				"population", SortOrder.DESC);
	}

	@Override
	public SearchResponse citySuggest(String asciiName, String suggestName) throws ElasticException {
		EsSuggest suggest = new EsSuggest(suggestName, asciiName, "", null);
		List<EsSuggest> listSuggest = Lists.newArrayList(suggest);

		CompletionBuilder completionBuilder = WooAggregationBuilderHelper.suggest(listSuggest);
		return entityManager.executeSuggest(masterConfig.getGeoCityIndexName(), completionBuilder);
	}

	@Override
	public List<CityEsBean> searchCityByGeoPoint(String geoPoint, String distance) throws Exception {
		List<CityEsBean> cityBeanList = new ArrayList<>();
		List<EsQuery> geoSearchQuery = new ArrayList<>();
		// Location location = new Location(43.70011,-79.4163);
		EsQuery query = new EsQuery(fieldConfig.getGeoLocation(), geoPoint, distance, EsModeQuery.GEO_DISTANCE,
				EsQueryIntMode.MUST);
		geoSearchQuery.add(query);
		
//		EsQuery rangeQuery = QueryBuilderHelper.createRangeQuery("population", "15000",
//				"1000", EsQueryIntMode.MUST);
//		geoSearchQuery.add(rangeQuery);
		
		QueryBuilder searchQueryBuilder = QueryBuilderHelper.search(geoSearchQuery, EsQueryIntMode.FILTER);
		SearchResponse response = entityManager.executeQuery(searchQueryBuilder, null, 0, 10,
				masterConfig.getGeoCityIndexName(), "population", SortOrder.DESC);
		SearchHit[] dataList = response.getHits().getHits();
		for (SearchHit hit : dataList) {
			cityBeanList.add(EntityMapper.getInstance().getObject(hit.getSourceAsString(), CityEsBean.class));
		}
		return cityBeanList;
	}

}
