package com.woocation.dao;

import java.util.List;

import com.woocation.model.BookingHotel;

/**
 * The Interface GeoDataSearchDao.
 */
public interface HotelSearchDao {

	/**
	 * Search hotel by geo point.
	 *
	 * @param hotelText
	 *            the hotel text
	 * @param geoPoint
	 *            the geo point
	 * @param distance
	 *            the distance
	 * @return the list
	 * @throws Exception
	 *             the exception
	 */
	public List<BookingHotel> searchHotelByGeoPoint(final String hotelText, final String geoPoint,
			final String distance) throws Exception;

	/**
	 * Hotel suggest.
	 *
	 * @param hotelText
	 *            the hotel text
	 * @return the list
	 * @throws Exception
	 *             the exception
	 */
	public List<BookingHotel> hotelSuggest(String hotelText) throws Exception;

}
