package com.woocation.dto;

import java.math.BigInteger;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.woocation.model.Location;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The Class CityDTO.
 */
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@NoArgsConstructor
public class CityDTO {

	/** The geoname id. */
	private long geonameId;

	/** The name. */
	private String name;

	/** The ascii name. */
	private String asciiName;

	/** The alternate names. */
	private String alternateNames;

	/** The lattitude. */
	private Location geoLocation;

	/** The country code. */
	private String countryCode;

	/** The state. */
	private String state;

	/** The country name. */
	private String countryName;

	/** The currency name. */
	private String currencyName;

	/** The currency code. */
	private String currencyCode;

	/** The admin 1 code. */
	private String admin1Code;

	/** The european. */
	private boolean european;

	/** The population. */
	private BigInteger population;

	/** The schengen. */
	private boolean schengen;

	/** The time zone. */
	private String timeZone;

	/** The time zone utc. */
	private String timeZoneUtc;

	/** The working days. */
	private String workingDays;
	
	/** The cab details. */
	private List<String> cabDetails;
	
	/** The threat ratio. */
	private Double threatRatio;
	
	/** The network operator. */
	private List<String> networkOperator;
	
	/** The google subway. */
	private Boolean googleSubway;

}
