package com.woocation.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.woocation.model.Location;

@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class GeoDTO {

	/** The geoname id. */
	private long geonameId;

	/** The ascii name. */
	private String asciiName;
	
	/** The lattitude. */
	private Location geoLocation;
	
	/** The country code. */
	private String countryCode;
	
	/** The state. */
	private String state;

	/** The admin 1 code. */
	private String admin1Code;
	
	/** The country name. */
	private String countryName;
	
	public GeoDTO() {
	}

	/**
	 * @return the geonameId
	 */
	public long getGeonameId() {
		return geonameId;
	}

	/**
	 * @param geonameId the geonameId to set
	 */
	public void setGeonameId(long geonameId) {
		this.geonameId = geonameId;
	}

	/**
	 * @return the asciiName
	 */
	public String getAsciiName() {
		return asciiName;
	}

	/**
	 * @param asciiName the asciiName to set
	 */
	public void setAsciiName(String asciiName) {
		this.asciiName = asciiName;
	}

	/**
	 * @return the geoLocation
	 */
	public Location getGeoLocation() {
		return geoLocation;
	}

	/**
	 * @param geoLocation the geoLocation to set
	 */
	public void setGeoLocation(Location geoLocation) {
		this.geoLocation = geoLocation;
	}

	/**
	 * @return the countryCode
	 */
	public String getCountryCode() {
		return countryCode;
	}

	/**
	 * @param countryCode the countryCode to set
	 */
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the countryName
	 */
	public String getCountryName() {
		return countryName;
	}

	/**
	 * @param countryName the countryName to set
	 */
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	/**
	 * @return the admin1Code
	 */
	public String getAdmin1Code() {
		return admin1Code;
	}

	/**
	 * @param admin1Code the admin1Code to set
	 */
	public void setAdmin1Code(String admin1Code) {
		this.admin1Code = admin1Code;
	}
}
