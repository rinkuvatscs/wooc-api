package com.woocation.dto;

import lombok.Data;

/**
 * Instantiates a new hotel suggest DTO.
 */
@Data
public class HotelSuggestDTO {

	/** The hotel id. */
	private Object hotelId;

	/** The location. */
	private Object location;
	
	/** The name. */
	private String name;

	/** The city. */
	private String city;

	/** The country. */
	private String country;
	
	/** The suggest key. */
	private String suggestKey;

}
