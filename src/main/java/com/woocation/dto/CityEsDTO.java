package com.woocation.dto;

import java.util.List;

import com.woocation.model.Airport;
import com.woocation.model.Elevation;
import com.woocation.model.Holiday;
import com.woocation.model.Languages;
import com.woocation.model.Network;
import com.woocation.model.Population;
import com.woocation.model.Subway;
import com.woocation.model.UVBean;
import com.woocation.model.Vegetation;
import com.woocation.model.Weather;

/**
 * The Class CityEsBean.
 */
public class CityEsDTO extends CityDTO {

	/** The subway dto. */
	private Subway subwayDto;

	/** The elevation ref dto. */
	private Elevation elevationRefDto;

	/** The network dto. */
	private Network networkDto;

	/** The population ref dto. */
	private Population populationRefDto;

	/** The languages ref dto. */
	private Languages languagesRefDto;

	/** The uv dto. */
	private UVBean uvDto;

	/** The vegetation dto. */
	private Vegetation vegetationDto;

	/** The weather dto. */
	private Weather weatherDto;

	/** The holidays. */
	private List<Holiday> holidays;

	/** The airports. */
	private List<Airport> airports;

	/**
	 * Instantiates a new city es bean.
	 */
	public CityEsDTO() {
	}

	/**
	 * Gets the subway dto.
	 *
	 * @return the subwayDto
	 */
	public Subway getSubwayDto() {
		return subwayDto;
	}

	/**
	 * Sets the subway dto.
	 *
	 * @param subwayDto
	 *            the subwayDto to set
	 */
	public void setSubwayDto(Subway subwayDto) {
		this.subwayDto = subwayDto;
	}

	/**
	 * Gets the elevation ref dto.
	 *
	 * @return the elevationRefDto
	 */
	public Elevation getElevationRefDto() {
		return elevationRefDto;
	}

	/**
	 * Sets the elevation ref dto.
	 *
	 * @param elevationRefDto
	 *            the elevationRefDto to set
	 */
	public void setElevationRefDto(Elevation elevationRefDto) {
		this.elevationRefDto = elevationRefDto;
	}

	/**
	 * Gets the network dto.
	 *
	 * @return the networkDto
	 */
	public Network getNetworkDto() {
		return networkDto;
	}

	/**
	 * Sets the network dto.
	 *
	 * @param networkDto
	 *            the networkDto to set
	 */
	public void setNetworkDto(Network networkDto) {
		this.networkDto = networkDto;
	}

	/**
	 * Gets the population ref dto.
	 *
	 * @return the populationRefDto
	 */
	public Population getPopulationRefDto() {
		return populationRefDto;
	}

	/**
	 * Sets the population ref dto.
	 *
	 * @param populationRefDto
	 *            the populationRefDto to set
	 */
	public void setPopulationRefDto(Population populationRefDto) {
		this.populationRefDto = populationRefDto;
	}

	/**
	 * Gets the languages ref dto.
	 *
	 * @return the languagesRefDto
	 */
	public Languages getLanguagesRefDto() {
		return languagesRefDto;
	}

	/**
	 * Sets the languages ref dto.
	 *
	 * @param languagesRefDto
	 *            the languagesRefDto to set
	 */
	public void setLanguagesRefDto(Languages languagesRefDto) {
		this.languagesRefDto = languagesRefDto;
	}

	/**
	 * Gets the uv dto.
	 *
	 * @return the uvDto
	 */
	public UVBean getUvDto() {
		return uvDto;
	}

	/**
	 * Sets the uv dto.
	 *
	 * @param uvDto
	 *            the uvDto to set
	 */
	public void setUvDto(UVBean uvDto) {
		this.uvDto = uvDto;
	}

	/**
	 * Gets the vegetation dto.
	 *
	 * @return the vegetationDto
	 */
	public Vegetation getVegetationDto() {
		return vegetationDto;
	}

	/**
	 * Sets the vegetation dto.
	 *
	 * @param vegetationDto
	 *            the vegetationDto to set
	 */
	public void setVegetationDto(Vegetation vegetationDto) {
		this.vegetationDto = vegetationDto;
	}

	/**
	 * Gets the weather dto.
	 *
	 * @return the weatherDto
	 */
	public Weather getWeatherDto() {
		return weatherDto;
	}

	/**
	 * Sets the weather dto.
	 *
	 * @param weatherDto
	 *            the weatherDto to set
	 */
	public void setWeatherDto(Weather weatherDto) {
		this.weatherDto = weatherDto;
	}

	/**
	 * @return the holidays
	 */
	public List<Holiday> getHolidays() {
		return holidays;
	}

	/**
	 * @param holidays
	 *            the holidays to set
	 */
	public void setHolidays(List<Holiday> holidays) {
		this.holidays = holidays;
	}

	/**
	 * @return the airports
	 */
	public List<Airport> getAirports() {
		return airports;
	}

	/**
	 * @param airports
	 *            the airports to set
	 */
	public void setAirports(List<Airport> airports) {
		this.airports = airports;
	}

}
