package com.woocation.elastic.bean;

import java.util.List;

import org.elasticsearch.search.aggregations.bucket.terms.Terms.Order;

import com.woocation.elastic.enums.AggregationOpsEnum;

import lombok.Data;

@Data
public class EsAggregation {

	/** The field name. */
	private String fieldname;

	/** The field label. */
	private String fieldLabel;

	/** The value of field. */
	private int size;

	/** The order. */
	private Order order;

	/** The next aggregation. */
	private EsAggregation nextAggregation;

	/** The aggregation operator. */
	private AggregationOpsEnum aggregationOperator;

	/** The filter on values. */
	private List<String> filterOnValues;

	/**
	 * Default constructor.
	 * 
	 * @param fieldname
	 *            the field name
	 * @param size
	 *            the size of result.
	 */
	public EsAggregation(String fieldname, int size) {
		this(fieldname, size, null);
	}

	/**
	 * Instantiates a new es aggregation.
	 *
	 * @param fieldname
	 *            the fieldname
	 * @param size
	 *            the size
	 * @param aggregationOperator
	 *            the aggregation operator
	 */
	public EsAggregation(String fieldname, int size, AggregationOpsEnum aggregationOperator) {
		this(fieldname, fieldname, size, aggregationOperator);
	}

	/**
	 * Instantiates a new es aggregation.
	 *
	 * @param fieldname
	 *            the fieldname
	 * @param fieldLabel
	 *            the field label
	 * @param size
	 *            the size
	 * @param aggregationOperator
	 *            the aggregation operator
	 */
	public EsAggregation(String fieldname, String fieldLabel, int size, AggregationOpsEnum aggregationOperator) {
		super();
		this.fieldname = fieldname;
		this.fieldLabel = fieldLabel;
		this.size = size;
		this.aggregationOperator = aggregationOperator;
	}

	/**
	 * Instantiates a new logs es aggregation.
	 *
	 * @param fieldname
	 *            the fieldname
	 * @param oper
	 *            the oper
	 * @param filterOnValues
	 *            the filter on values
	 */
	public EsAggregation(String fieldname, AggregationOpsEnum oper, List<String> filterOnValues) {
		this(fieldname, fieldname, oper, filterOnValues);
	}

	/**
	 * Instantiates a new logs es aggregation.
	 *
	 * @param fieldname
	 *            the fieldname
	 * @param fieldLabel
	 *            the field label
	 * @param oper
	 *            the oper
	 * @param filterOnValues
	 *            the filter on values
	 */
	public EsAggregation(String fieldname, String fieldLabel, AggregationOpsEnum oper, List<String> filterOnValues) {
		this(fieldname, fieldLabel, -1, oper);
		this.filterOnValues = filterOnValues;
	}

}
