package com.woocation.elastic.builder;

import java.util.HashMap;

import org.elasticsearch.search.suggest.completion.CompletionSuggestionBuilder;

/**
 * The Class CompletionBuilder.
 */
public class CompletionBuilder extends HashMap<String, CompletionSuggestionBuilder> {

    /**
     * The serial version.
     */
    private static final long serialVersionUID = 5459477721339920037L;

    /**
     * Default constructor.
     */
    public CompletionBuilder() {
        super();
    }
}
